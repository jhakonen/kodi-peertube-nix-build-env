# Closest Boost version to v1.64 that compiles with libtorrent-rasterbar

{ pkgs, fetchurl }:
if (builtins.hasAttr "boost166" pkgs)
  then
    pkgs.boost166
  else
    # In Nixos 23.11 Boost v1.66 is no more, use self-build version instead
    pkgs.boost.overrideAttrs(attrs: {
      src = fetchurl {
        url = "mirror://sourceforge/boost/boost_1_66_0.tar.bz2";
        sha256 = "sha256-VyGBglPmoJiVgxkvlngsSpjrYgSWUxbfn1rXWBkiXKk=";
      };
      version = "1.66_0";
      patches = [];
    })
