{ stdenv, lib, fetchFromGitHub, pkg-config, automake, autoconf
, my-zlib, my-boost, my-openssl, libtool, libiconv, geoip, ncurses
}:

let
  version = "1.1.11";
  formattedVersion = lib.replaceChars ["."] ["_"] version;

in stdenv.mkDerivation {
  name = "libtorrent-rasterbar-${version}";

  src = fetchFromGitHub {
    owner = "arvidn";
    repo = "libtorrent";
    rev = "libtorrent_${formattedVersion}";
    sha256 = "0nwdsv6d2gkdsh7l5a46g6cqx27xwh3msify5paf02l1qzjy4s5l";
  };

  enableParallelBuilding = true;
  nativeBuildInputs = [ automake autoconf libtool pkg-config ];
  buildInputs = [ my-boost my-openssl my-zlib libiconv geoip ncurses ];
  preConfigure = "./autotool.sh";

  postInstall = ''
    moveToOutput "include" "$dev"
  '';

  outputs = [ "out" "dev" ];

  configureFlags = [
    "--with-libgeoip=system"
    "--with-libiconv=yes"
    "--with-boost=${my-boost.dev}"
    "--with-boost-libdir=${my-boost.out}/lib"
    "--with-libiconv=yes"
    "--enable-debug=yes"
  ];

  meta = with lib; {
    homepage = "https://libtorrent.org/";
    description = "A C++ BitTorrent implementation focusing on efficiency and scalability";
    license = licenses.bsd3;
    maintainers = [ maintainers.phreedom ];
    platforms = platforms.unix;
  };
}
