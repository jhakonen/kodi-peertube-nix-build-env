{ kodi }:
kodi.withPackages (exts: [
  exts.requests
  exts.signals
  (exts.callPackage ./kodi-addons/vfs-libtorrent.nix {})
  (exts.callPackage ./kodi-addons/peertube.video.plugin.nix {})
])
