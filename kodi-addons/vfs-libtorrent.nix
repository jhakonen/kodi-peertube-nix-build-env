{ addonDir
, buildKodiBinaryAddon
, callPackage
, fetchFromGitLab
, fetchurl
, lib
, openssl_1_1
, pkg-config
, pkgs
, zlib
}:
let
  my-boost = callPackage ../dependencies/boost-1.66.nix {};
  my-libtorrent-rasterbar = callPackage ../dependencies/libtorrent-rasterbar-1.1.11.nix {
    inherit my-boost;
    inherit my-openssl;
    inherit my-zlib;
  };
  my-openssl = openssl_1_1.overrideAttrs(attrs: {
    version = "1.1.1g";
  });
  my-zlib = zlib.overrideAttrs(attrs: {
    version = "1.2.11";
    src = fetchurl {
      url = "https://www.zlib.net/fossils/zlib-1.2.11.tar.gz";
      hash = "sha256-w+Xp/dUATctUL+2l7k8P8HRGKLr47S3V1m+MoRl8saE=";
    };
  });
in
buildKodiBinaryAddon rec {
  pname = "vfs-libtorrent";
  namespace = "vfs.libtorrent";
  version = "0.1.0-alpha";

  src = fetchFromGitLab {
    owner = "jhakonen";
    repo = "vfs.libtorrent";
    domain = "framagit.org";
    rev = "kodi-with-matrix-and-nexus-plus-fixes";
    hash = "sha256-qqgSBsqdFx3SK2emu7nzpQt6CmMo6cc4GNZEKHFe9uA=";
  };
  # For local hacking
  # src = /home/jhakonen/code/kodi-peertube/vfs.libtorrent;

  extraBuildInputs = [ my-zlib my-openssl my-boost my-libtorrent-rasterbar ];
  extraNativeBuildInputs = [ pkg-config ];

  extraCMakeFlags = [ "-DCMAKE_BUILD_TYPE=Debug" ];

  extraInstallPhase = let n = namespace; in ''
    ln -s $out/lib/addons/${n}/vfs.libtorrent.so.0.1.0 $out/${addonDir}/${n}/vfs.libtorrent.so.0.1.0
  '';

  preConfigure = ''
    substituteInPlace CMakeLists.txt \
      --replace "find_package(LibtorrentRasterbar REQUIRED)" \
      "
      find_package(PkgConfig REQUIRED)
      pkg_check_modules(LibtorrentRasterbar REQUIRED IMPORTED_TARGET libtorrent-rasterbar)
      "

    substituteInPlace CMakeLists.txt \
      --replace "#             \''${LibtorrentRasterbar_LIBRARIES}" "            \''${LibtorrentRasterbar_LIBRARIES}" \
      --replace "#             \''${Boost_LIBRARIES}"               "            \''${Boost_LIBRARIES}" \
      --replace "\''${CMAKE_PREFIX_PATH}/lib/libboost_chrono.a" "" \
      --replace "\''${CMAKE_PREFIX_PATH}/lib/libboost_date_time.a" ""  \
      --replace "\''${CMAKE_PREFIX_PATH}/lib/libboost_random.a" ""  \
      --replace "\''${CMAKE_PREFIX_PATH}/lib/libboost_system.a" ""  \
      --replace "\''${CMAKE_PREFIX_PATH}/lib/libtorrent-rasterbar.a" "" \
  '';

  meta = with lib; {
    homepage = "https://framagit.org/thombet/vfs.libtorrent";
    description = "Support of the BitTorrent protocol with libtorrent";
    platforms = platforms.all;
  };
}