{ buildKodiAddon
, fetchFromGitLab
, lib
, requests
, signals
, vfs-libtorrent
}:
buildKodiAddon rec {
  pname = "plugin-video-peertube";
  namespace = "plugin.video.peertube";
  version = "1.2.0";

  src = fetchFromGitLab {
    owner = "jhakonen";
    repo = "plugin.video.peertube";
    domain = "framagit.org";
    rev = "fixed-vfs-libtorrent-plus-matrix";
    hash = "sha256-TlqfK9Bw4xi3O/UZbKH+8LeMrbMpPGBgiyZ21D/rXEg=";
  };
  # For local hacking
  # src = /home/jhakonen/code/kodi-peertube/plugin.video.peertube;

  # Extra Kodi addons that this addon depends on
  propagatedBuildInputs = [ requests signals vfs-libtorrent ];

  meta = with lib; {
    homepage = "https://framagit.org/StCyr/plugin.video.peertube";
    description = "Add-on for PeerTube";
  };
}
