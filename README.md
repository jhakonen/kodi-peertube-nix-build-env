# Testing environment for Kodi PeerTube addon

Run Kodi Nexus with Peertube addon installed:
```bash
NIXPKGS_ALLOW_INSECURE=1 nix run .#kodi-nexus-with-peertube --impure -- --logging=console
```

Run Kodi Matrix with Peertube addon installed:
```bash
NIXPKGS_ALLOW_INSECURE=1 nix run .#kodi-matrix-with-peertube --impure -- --logging=console
```

Usage as part of a flake in NixOS (not tested):
```nix
{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    kodi-peertube.url = "gitlab:jhakonen/kodi-peertube-nix-build-env?host=framagit.org";
    kodi-peertube.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, kodi-peertube, nixpkgs }:
    {
      packages.x86_64-linux.default = 
        let
          pkgs = import nixpkgs { system = "x86_64-linux"; };
        in
          pkgs.kodi.withPackages (exts: [
            (kodi-peertube.create-peertube-addon exts)
          ]);
    };
}
```
