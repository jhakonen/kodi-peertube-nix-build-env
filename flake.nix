{
  description = "Flake with Kodi PeerTube addon";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    nixpkgs-22.url = "github:nixos/nixpkgs/nixos-22.11";
    nixgl-22.url = "github:nix-community/nixGL";
    nixgl-22.inputs.nixpkgs.follows = "nixpkgs-22";
  };

  outputs = { self, nixpkgs, nixpkgs-22, nixgl-22 }:
    let
      create-vfs-libtorrent-addon = exts: exts.callPackage ./kodi-addons/vfs-libtorrent.nix {};
      create-peertube-addon = exts: exts.callPackage ./kodi-addons/peertube.video.plugin.nix {
        vfs-libtorrent = create-vfs-libtorrent-addon exts;
      };
    in
    {
      inherit create-peertube-addon;

      packages.x86_64-linux.kodi-nexus-with-peertube = 
        let
          pkgs = import nixpkgs { system = "x86_64-linux"; };
        in
          pkgs.kodi.withPackages (exts: [ (create-peertube-addon exts) ]);

      packages.x86_64-linux.kodi-matrix-with-peertube = 
        let
          pkgs = import nixpkgs { system = "x86_64-linux"; };
          pkgs-22 = import nixpkgs-22 {
            system = "x86_64-linux";
            overlays = [ nixgl-22.overlay ];
          };
          kodi = pkgs-22.kodi.withPackages (exts: [ (create-peertube-addon exts) ]);
        in
          pkgs.writeShellScriptBin "kodi-matrix" ''
            ${pkgs-22.nixgl.auto.nixGLDefault}/bin/nixGL ${kodi}/bin/kodi "$@"
            if [[ "$@" == *"--logging=console"* ]]; then
              ${pkgs.coreutils}/bin/cat ~/.kodi/temp/kodi.log
            fi
          '';
    };
}
